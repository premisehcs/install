Premise Home Control Software is a free solution integrating lighting, security, and A/V media into one cohesive, automated system. Read more on the [wiki](https://gitlab.com/premise/install/wikis/home). 

# Install
On modern version of Windows (later than Windows 7) the version 2.1 installer needs a little coaxing to install correctly. Follow these steps:

1. Download and run the installer from [here](https://gitlab.com/premise/install/tree/master).
2. End the install application under the application tab in task manager. 
2. Next, I manually ensure the premise services under the services tab in task manager is stopped. Don't stop the individual installer processes (under the task manager process tab) as this will uninstall premise and remove it. 
3. Reboot. Makes sure the Premise services are stopped. Click on the setup.exe again, but this time select "repair."
4. Reboot again and ensure the premise services are started (they should be).